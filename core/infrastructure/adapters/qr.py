from io import BytesIO

import qrcode

from core.domain.entities.qr import Qr
from core.interactor.dtos.qr import MakeQrInputDto


class QrMakerAdapter:
    def make_qr(self, input_dto: MakeQrInputDto) -> Qr:
        img = qrcode.make(
            input_dto.url,
            version=3,
            box_size=10,
            border=4,
        )
        img_bytes = BytesIO()
        img.save(img_bytes)
        img_bytes.seek(0)

        return Qr(code=img_bytes)
