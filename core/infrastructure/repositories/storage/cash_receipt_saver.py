import time
from datetime import datetime
from zoneinfo import ZoneInfo

import pdfkit
from jinja2 import Environment, PackageLoader

from config import MEDIA_DIR
from core.domain.entities.cash_receipt import CashReceipt
from core.domain.entities.item import Item


class CashReceiptSaverStorageRepository:
    def save_cash_receipt(self, items: tuple[Item, ...]) -> CashReceipt:
        cash_receipt_id = int(time.time())
        now = datetime.now(ZoneInfo('Europe/Moscow'))

        cash_receipt = CashReceipt(
            id_=cash_receipt_id,
            date_time=now.strftime('%d.%m.%Y %H:%M'),
            items=items,
        )
        html_string = self._make_html_string(cash_receipt)
        self._convert_to_pdf(html_string, str(cash_receipt_id))

        return cash_receipt

    def _convert_to_pdf(self, html_string: str, filename: str) -> None:
        options = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': 'UTF-8',
            'no-outline': None,
        }
        pdfkit.from_string(
            html_string, f'{MEDIA_DIR}/{filename}.pdf', options=options
        )

    def _make_html_string(self, cash_receipt: CashReceipt) -> str:
        env = Environment(loader=PackageLoader('core', 'templates'))
        data = {
            'datetime': cash_receipt.date_time,
            'items': cash_receipt.items,
            'total_price': cash_receipt.total_price,
        }
        template = env.get_template('cash_receipt.html')
        output = template.render(data)
        return output
