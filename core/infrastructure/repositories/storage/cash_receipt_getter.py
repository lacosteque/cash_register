from pathlib import Path

from config import MEDIA_DIR
from core.domain.entities.cash_receipt import CashReceiptFile


class CashReceiptGetterRepository:
    def get_cash_receipt_by_filename(self, filename: str) -> CashReceiptFile:
        content = self._file_open(filename)
        return CashReceiptFile(content=content)

    def _file_open(self, filename: str) -> bytes:
        file = Path(f'{MEDIA_DIR}/{filename}').open('rb')
        return file.read()
