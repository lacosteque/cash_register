from random import choice

from mimesis import Finance, Generic
from mimesis.locales import Locale

from core.domain.entities.item import Item

g = Generic(locale=Locale.RU)
f = Finance()


class ItemGetterMemoryRepository:
    def __init__(self) -> None:
        self.items_in_memory: dict[int, Item] = {
            num: Item(
                id_=num,
                title=choice(
                    (
                        g.food.vegetable(),
                        g.food.fruit(),
                        g.food.dish(),
                        g.food.drink(),
                    )
                ),
                price=f.price(),
            )
            for num in range(100)
        }

    def get_items_by_ids(self, ids: list[int]) -> tuple[Item, ...]:
        return tuple(
            result for id in ids if (result := self.items_in_memory.get(id))
        )
