from core.interactor.dtos.cash_receipt import GetCashReceiptFileOutputDto


class CashReceiptFileGetterPresenter:
    def present(self, output_dto: GetCashReceiptFileOutputDto) -> bytes:
        return output_dto.cash_receipt.content
