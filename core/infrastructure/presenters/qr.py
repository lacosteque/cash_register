from core.interactor.dtos.qr import MakeQrOutputDto


class QrMakerPresenter:
    def present(self, output_dto: MakeQrOutputDto) -> bytes:
        return output_dto.code.code.getvalue()
