from typing import Iterable, NamedTuple

from config import DOMAIN
from core.domain.entities.item import Item


class CashReceiptFile(NamedTuple):
    content: bytes


class CashReceipt(NamedTuple):
    id_: int
    date_time: str
    items: Iterable[Item]

    @property
    def url(self) -> str:
        return f'http://{DOMAIN}/media/{self.id_}.pdf'

    @property
    def total_price(self) -> float:
        return sum([item.price for item in self.items])
