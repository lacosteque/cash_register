from typing import NamedTuple


class Item(NamedTuple):
    id_: int
    title: str
    price: float
