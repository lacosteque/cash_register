from io import BytesIO
from typing import NamedTuple


class Qr(NamedTuple):
    code: BytesIO
