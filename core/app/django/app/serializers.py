from rest_framework import serializers


class AppSerializer(serializers.Serializer):
    items = serializers.ListField(child=serializers.IntegerField())
