from app.controllers import CashReceiptFileGetterController, QrMakerController
from app.serializers import AppSerializer
from django.http import HttpResponse
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from core.interactor.dtos.cash_receipt import GetCashReceiptFileInputDto
from core.interactor.dtos.item import FetchItemsInputDto


class CashReceiptAPIView(APIView):
    def get(self, request: Request, filename: str) -> Response | HttpResponse:
        input_dto = GetCashReceiptFileInputDto(filename)
        controller = CashReceiptFileGetterController()
        result = controller.execute(input_dto)
        if not result:
            return Response('Zzzz...', status=status.HTTP_400_BAD_REQUEST)

        return HttpResponse(result, content_type='application/octet-stream')


class QrAPIView(APIView):
    def post(
        self, request: Request, *args, **kwargs
    ) -> Response | HttpResponse:  # noqa: E999
        serializer = AppSerializer(data=request.data)
        if serializer.is_valid():
            items = serializer.validated_data.get('items')  # pyright: ignore
            controller = QrMakerController()
            input_dto = FetchItemsInputDto(items)  # pyright: ignore
            result = controller.execute(input_dto)

            if not result:
                return Response('Zzzz...', status=status.HTTP_400_BAD_REQUEST)

            return HttpResponse(
                result, content_type='application/octet-stream'
            )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
