from core.infrastructure.adapters.qr import QrMakerAdapter
from core.infrastructure.presenters.cash_receipt import (
    CashReceiptFileGetterPresenter,
)
from core.infrastructure.presenters.qr import QrMakerPresenter
from core.infrastructure.repositories.memory.items_getter import (
    ItemGetterMemoryRepository,
)
from core.infrastructure.repositories.storage.cash_receipt_getter import (
    CashReceiptGetterRepository,
)
from core.infrastructure.repositories.storage.cash_receipt_saver import (
    CashReceiptSaverStorageRepository,
)
from core.interactor.dtos.cash_receipt import GetCashReceiptFileInputDto
from core.interactor.dtos.item import FetchItemsInputDto
from core.interactor.use_cases.get_cash_receipt import GetCashReceiptUseCase
from core.interactor.use_cases.make_qr_cash_receipt import (
    MakeQrCashReceiptUseCase,
)


class CashReceiptFileGetterController:
    def execute(self, input_dto: GetCashReceiptFileInputDto) -> bytes | None:
        repository = CashReceiptGetterRepository()
        presenter = CashReceiptFileGetterPresenter()
        use_case = GetCashReceiptUseCase(
            repository=repository, presenter=presenter
        )
        result = use_case.execute(input_dto=input_dto)
        return result


class QrMakerController:
    def execute(self, input_dto: FetchItemsInputDto) -> bytes | None:
        items_repository = ItemGetterMemoryRepository()
        cash_receipt_repository = CashReceiptSaverStorageRepository()
        adapter = QrMakerAdapter()
        presenter = QrMakerPresenter()

        use_case = MakeQrCashReceiptUseCase(
            items_repository=items_repository,
            cash_receipt_repository=cash_receipt_repository,
            adapter=adapter,
            presenter=presenter,
        )
        result = use_case.execute(input_dto)
        return result
