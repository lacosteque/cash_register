from typing import Protocol

from core.interactor.dtos.qr import MakeQrOutputDto


class QrMakerPresenterInterface(Protocol):
    def present(self, output_dto: MakeQrOutputDto) -> bytes:
        ...
