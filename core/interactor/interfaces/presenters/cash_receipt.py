from typing import Protocol

from core.interactor.dtos.cash_receipt import GetCashReceiptFileOutputDto


class CashReceiptFileGetterPresenterInterface(Protocol):
    def present(self, output_dto: GetCashReceiptFileOutputDto) -> bytes:
        ...
