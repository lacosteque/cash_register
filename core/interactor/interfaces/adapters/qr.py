from typing import Protocol

from core.domain.entities.qr import Qr
from core.interactor.dtos.qr import MakeQrInputDto


class QrMakerAdapterInterface(Protocol):
    def make_qr(self, input_dto: MakeQrInputDto) -> Qr:
        ...
