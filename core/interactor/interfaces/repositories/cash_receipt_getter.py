from typing import Protocol

from core.domain.entities.cash_receipt import CashReceiptFile


class CashReceiptGetterRepositoryInterface(Protocol):
    def get_cash_receipt_by_filename(self, filename: str) -> CashReceiptFile:
        ...
