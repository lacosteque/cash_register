from typing import Protocol

from core.domain.entities.cash_receipt import CashReceipt
from core.domain.entities.item import Item


class CashReceiptSaverRepositoryInterface(Protocol):
    def save_cash_receipt(self, items: tuple[Item, ...]) -> CashReceipt:
        ...
