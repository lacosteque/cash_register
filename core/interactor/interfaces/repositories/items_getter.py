from typing import Protocol

from core.domain.entities.item import Item


class ItemGetterRepositoryInterface(Protocol):
    def get_items_by_ids(self, ids: list[int]) -> tuple[Item, ...]:
        ...
