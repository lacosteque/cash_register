from typing import NamedTuple

from core.domain.entities.cash_receipt import CashReceiptFile


class GetCashReceiptFileInputDto(NamedTuple):
    filename: str


class GetCashReceiptFileOutputDto(NamedTuple):
    cash_receipt: CashReceiptFile
