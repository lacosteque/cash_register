from typing import NamedTuple


class FetchItemsInputDto(NamedTuple):
    ids: list[int]
