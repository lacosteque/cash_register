from typing import NamedTuple

from core.domain.entities.qr import Qr


class MakeQrInputDto(NamedTuple):
    url: str


class MakeQrOutputDto(NamedTuple):
    code: Qr
