from core.interactor.dtos.item import FetchItemsInputDto
from core.interactor.dtos.qr import MakeQrInputDto, MakeQrOutputDto
from core.interactor.interfaces.adapters.qr import QrMakerAdapterInterface
from core.interactor.interfaces.presenters.qr import QrMakerPresenterInterface
from core.interactor.interfaces.repositories.cash_receipt_saver import (
    CashReceiptSaverRepositoryInterface,
)
from core.interactor.interfaces.repositories.items_getter import (
    ItemGetterRepositoryInterface,
)


class MakeQrCashReceiptUseCase:
    def __init__(
        self,
        items_repository: ItemGetterRepositoryInterface,
        cash_receipt_repository: CashReceiptSaverRepositoryInterface,
        adapter: QrMakerAdapterInterface,
        presenter: QrMakerPresenterInterface,
    ) -> None:
        self.items_repository = items_repository
        self.cash_receipt_repository = cash_receipt_repository
        self.adapter = adapter
        self.presenter = presenter

    def execute(self, input_dto: FetchItemsInputDto) -> bytes | None:
        items = self.items_repository.get_items_by_ids(ids=input_dto.ids)

        if not items:
            return None

        cash_receipt = self.cash_receipt_repository.save_cash_receipt(
            items=items
        )

        qr_input_dto = MakeQrInputDto(url=cash_receipt.url)
        qr_code = self.adapter.make_qr(input_dto=qr_input_dto)
        qr_output_dto = MakeQrOutputDto(code=qr_code)
        presenter_response = self.presenter.present(output_dto=qr_output_dto)
        return presenter_response
