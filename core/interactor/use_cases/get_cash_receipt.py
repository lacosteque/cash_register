from core.interactor.dtos.cash_receipt import (
    GetCashReceiptFileInputDto,
    GetCashReceiptFileOutputDto,
)
from core.interactor.interfaces.presenters.cash_receipt import (
    CashReceiptFileGetterPresenterInterface,
)
from core.interactor.interfaces.repositories.cash_receipt_getter import (
    CashReceiptGetterRepositoryInterface,
)


class GetCashReceiptUseCase:
    def __init__(
        self,
        repository: CashReceiptGetterRepositoryInterface,
        presenter: CashReceiptFileGetterPresenterInterface,
    ) -> None:
        self.repository = repository
        self.presenter = presenter

    def execute(self, input_dto: GetCashReceiptFileInputDto) -> bytes | None:
        cash_receipt = self.repository.get_cash_receipt_by_filename(
            filename=input_dto.filename
        )

        if not cash_receipt:
            return None

        output_dto = GetCashReceiptFileOutputDto(cash_receipt=cash_receipt)
        presenter_response = self.presenter.present(output_dto=output_dto)
        return presenter_response
