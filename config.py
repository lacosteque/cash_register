import os

from dotenv import load_dotenv

load_dotenv(verbose=True)


DOMAIN = os.getenv('DOMAIN', 'localhost:8000')

BASE_DIR_PATH = os.path.dirname(os.path.abspath(__file__))

MEDIA_DIR = '/'.join((BASE_DIR_PATH, 'core', 'media'))
