FROM python:3.12.1-slim-bookworm as base

ENV PYTHONUNBUFFERED=1 
ENV PYTHONDONTWRITEBYTECODE=1 
ENV PIP_NO_CACHE_DIR=off 
ENV PIP_DISABLE_PIP_VERSION_CHECK=on 
ENV PIP_DEFAULT_TIMEOUT=100 
ENV POETRY_VERSION=1.7.0 
ENV POETRY_HOME="/opt/poetry" 
ENV POETRY_VIRTUALENVS_IN_PROJECT=true 
ENV POETRY_NO_INTERACTION=1 
ENV PYSETUP_PATH="/opt/pysetup" 
ENV VENV_PATH="/opt/pysetup/.venv"
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

FROM base as builder
RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install --no-install-recommends -y curl

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml ./

RUN poetry install --no-dev

FROM base as runtime

RUN apt-get update \
  && apt-get install --no-install-recommends -y wkhtmltopdf \
  && apt clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY --from=builder $PYSETUP_PATH $PYSETUP_PATH

COPY core core
COPY config.py config.py

EXPOSE 8000

CMD ["python", "/app/core/app/django/manage.py", "runserver", "0.0.0.0:8000"]

