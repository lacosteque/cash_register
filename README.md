

# Cash Register App

Данный проект выполнен в рамках тестового задания. Условия задания доступны [по ссылке](https://hail-dime-646.notion.site/Python-ee2cacfa76dc4810b218cfb904a36a89).

## Установка и запуск

Для развертывания приложения выполните следующие шаги:


1. Клонировать репозиторий:
``` 
 git clone https://gitlab.com/lacosteque/cash_register.git 
 ```
2. Перейти в директорию приложения
```
 cd cash_register
 ```
### Используя Docker

3. Собрать Docker образ:
```
docker build -t cash:latest .
```
4. Запустить приложение:
```
docker run -d -p 8000:8000 cash:latest
```

### Используя Docker Compose
3. Собрать Docker образ:
```
docker-compose build
```
4. Запустить приложение:
```
docker-compose up -d
```

